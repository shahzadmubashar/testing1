defmodule Testing1.Repo.Migrations.CreateRates do
  use Ecto.Migration

  def change do
    create table(:rates) do
      add :email, :string
      add :score, :integer
      add :product_id, references(:products, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:rates, [:email, :product_id], name: :unique_email_product_index)
    # create index(:rates, [:product_id])
  end
end
