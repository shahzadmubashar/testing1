# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Testing1.Repo.insert!(%Testing1.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Testing1.Rating.create_product(%{name: "Product1" , availability: 10})
Testing1.Rating.create_product(%{name: "Product2" , availability: 2})
Testing1.Rating.create_product(%{name: "Product3" , availability: 30})
Testing1.Rating.create_product(%{name: "Product4" , availability: 0})
Testing1.Rating.create_product(%{name: "Product5" , availability: 0})

Testing1.Rating.create_rate(%{email: "ahmed@email.com" , score: 3, product_id: 1})
Testing1.Rating.create_rate(%{email: "ahmed@email.com" , score: 4, product_id: 2})
Testing1.Rating.create_rate(%{email: "ahmed@email.com" , score: 4, product_id: 3})

Testing1.Rating.create_rate(%{email: "ali@email.com" , score: 5, product_id: 1})
Testing1.Rating.create_rate(%{email: "ali@email.com" , score: 5, product_id: 2})
Testing1.Rating.create_rate(%{email: "ali@email.com" , score: 5, product_id: 3})
