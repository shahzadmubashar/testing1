use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :testing1, Testing1Web.Endpoint,
  http: [port: 4001],
  server: true #false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :testing1, Testing1.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "123",
  database: "testing1_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox


config :hound, driver: "chrome_driver"
config :testing1, sql_sandbox: true
