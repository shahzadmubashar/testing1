# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :testing1,
  ecto_repos: [Testing1.Repo]

# Configures the endpoint
config :testing1, Testing1Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZohYkbq625G8pGKm+a41helJe3RiyaGk4kGP+yHS7K0g6rTW2umt80AEFv/hihFN",
  render_errors: [view: Testing1Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Testing1.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
