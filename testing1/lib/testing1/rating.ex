defmodule Testing1.Rating do


  import Ecto.Query, warn: false
  alias Testing1.Repo
  alias Testing1.Rating.Product
  alias Testing1.Rating.Rate

  def get_product!(id), do: Repo.get!(Product,id)

  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  def delete_all_products() do
    Repo.delete_all(Rate)
    Repo.delete_all(Product)
  end

  def list_rates do
    Repo.all(Rate)
  end

  def get_rate!(id), do: Repo.get!(Rate, id)

  def create_rate(attrs \\ %{}) do
    %Rate{}
    |> Rate.changeset(attrs)
    |> Repo.insert()
  end

  def list_products_sorted do
    query = from p in Product,
            left_join: r in Rate,
            on: r.product_id == p.id,
            group_by: p.id,
            select: %{id: p.id, name: p.name, availability: p.availability, score_count: count(r.score), avg_score: avg(r.score) },
            order_by: [desc: avg(r.score), asc: p.name]

    Repo.all(query)
  end

  def list_products_sorted(product_id) do
    query = from p in Product,
            left_join: r in Rate,
            on: r.product_id == p.id,
            where: p.id == ^product_id,
            group_by: p.id,
            select: %{id: p.id, name: p.name, availability: p.availability, score_count: count(r.score), avg_score: avg(r.score) },
            order_by: [desc: avg(r.score), asc: p.name]

    Repo.all(query)
  end

end
