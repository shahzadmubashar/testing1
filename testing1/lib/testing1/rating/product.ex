defmodule Testing1.Rating.Product do
  use Ecto.Schema
  import Ecto.Changeset


  schema "products" do
    field :availability, :integer
    field :name, :string
    has_many :rates, Testing1.Rating.Product

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :availability])
    |> validate_required([:name, :availability])
    |> unique_constraint(:name)
  end
end
