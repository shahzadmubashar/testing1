defmodule Testing1.Rating.Rate do
  use Ecto.Schema
  import Ecto.Changeset


  schema "rates" do
    field :email, :string
    field :score, :integer
    belongs_to :product, Testing1.Rating.Product
    # field :product_id, :id

    timestamps()
  end

  @doc false
  def changeset(rate, attrs) do
    rate
    |> cast(attrs, [:email, :score, :product_id])
    |> validate_required([:email, :score, :product_id])
    |> validate_inclusion(:score, 0..5)
    |> unique_constraint(:unique_email_product_index, name: :unique_email_product_index)
    |> foreign_key_constraint(:product, name: :rates_product_id_fkey, message: "Product doesn't exist")
  end
end
