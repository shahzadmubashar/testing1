defmodule Testing1Web.Router do
  use Testing1Web, :router


  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Testing1Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/list", RatingController, :index
    get "/list/new", ProductController, :new
    post "/list/create", ProductController, :create
  end

  # Other scopes may use custom stacks.
  # scope "/api", Testing1Web do
  #   pipe_through :api
  # end
end
