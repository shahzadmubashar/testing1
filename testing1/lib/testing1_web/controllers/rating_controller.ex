defmodule Testing1Web.RatingController do
  use Testing1Web, :controller

  alias Testing1.Rating


  def index(conn,_params) do
    products = Rating.list_products_sorted()
    render conn, "index.html", products: products
  end

  def new(conn, %{"product_id" => product_id}) do

    product = Rating.get_product!(product_id)
    case product.availability != 0 do
      true ->
        render conn, "new.html", product_id: product_id
      _else ->
        conn
        |> put_flash(:error, "You cant update non existing item.")
        |> redirect(to: rating_path(conn,:index))
      end
  end

  def create(conn, %{"product_id" => product_id,"email" => email,"score" => score}) do
      case Rating.create_rate(%{email: email , score: score, product_id: product_id}) do
        {:ok,_anything} ->
            conn
            |> put_flash(:info, "Score added successfully")
            |> redirect(to: rating_path(conn,:index))
        _else ->
            conn
            |> put_flash(:error, "Error in insering score")
            |> redirect(to: rating_path(conn,:new,%{product_id: product_id} ))
      end
  end

end
