Feature: Add User
    As admin
    Scuch that I go to users pag
    I want to add User to the system

    Scenario: Add a user with success
        Given the following users are available
            | name  | username |
            | demo  | demo     |
            | demo1 | demo1    |
        And I want to add add new user
        And I press add new user button
        And I enter name and username
        When I submit the request
        Then I should receive a confirmation message
         