defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state fn ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(ExamPrep.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(ExamPrep.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(ExamPrep.Repo)
    Hound.end_session
  end


  given_ ~r/^the following users are available$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I want to add add new user$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I press add new user button$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I enter name and username$/, fn state ->
    {:ok, state}
  end

  when_ ~r/^I submit the request$/, fn state ->
    {:ok, state}
  end

  then_ ~r/^I should receive a confirmation message$/, fn state ->
    {:ok, state}
  end

end
